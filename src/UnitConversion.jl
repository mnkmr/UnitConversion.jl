module UnitConversion

export unitconvert, energyconvert, massconvert, pressureconvert

using Printf: @sprintf

# Avogadro number (mol⁻¹)
const NA = 6.02214076e23
# Planck constant (Js)
const h = 6.62607015e-34
# Speed of light (ms⁻¹)
const c = 299792458


struct Energy
    J::Float64
    kJ::Float64
    Jmol::Float64
    kJmol::Float64
    cal::Float64
    kcal::Float64
    calmol::Float64
    kcalmol::Float64
    eV::Float64
    cm::Float64
    Hz::Float64
    MHz::Float64
    GHz::Float64
    nm::Float64
end

function Base.show(io::IO, ::MIME"text/plain", e::Energy)
    print("UnitConversion.Energy:\n")
    floatpoint_alignment([
        e.J       => "J",
        e.kJ      => "kJ",
        e.Jmol    => "J/mol",
        e.kJmol   => "kJ/mol",
        e.cal     => "cal",
        e.kcal    => "kcal",
        e.calmol  => "cal/mol",
        e.kcalmol => "kcal/mol",
        e.eV      => "eV",
        e.cm      => "cm⁻¹",
        e.Hz      => "Hz",
        e.MHz     => "MHz",
        e.GHz     => "GHz",
        e.nm      => "nm",
    ])
end

function floatpoint_alignment(pairs)
    maxprewidth, maxpostwidth = 0, 0
    parts = NTuple{3,Any}[]
    for (n, unit) in pairs
        pre, post = split(prettyprint(n), "."; limit=2)
        push!(parts, (pre, post, unit))
        maxprewidth = max(maxprewidth, length(pre))
        maxpostwidth = max(maxpostwidth, length(post))
    end
    width = maxprewidth + maxpostwidth + 1
    for i in 1:length(parts)
        pre, post, unit = parts[i]
        prespace = " "^(maxprewidth - length(pre))
        postspace = " "^(maxpostwidth - length(post))
        line = ["  ", prespace, pre, ".", post, postspace, " ", unit]
        if i != length(parts)
            push!(line, "\n")
        end
        print(join(line, ""))
    end
end

const FLOATMIN = Ref{Float64}(0.1)
const FLOATMAX = Ref{Float64}(100000.0)

function prettyprint(n)
    if FLOATMIN[] < n < FLOATMAX[]
        @sprintf("%10.3f", n)
    else
        @sprintf("%12.3e", n)
    end
end


function energyconvert(e, from::Symbol, to::Symbol)
    if from == to
        e
    else
        e |> to_joule(from) |> joule_to(to)
    end
end
function energyconvert(e, fromstr::AbstractString, tostr::AbstractString)
    from = energyunit_symbol(fromstr)
    to = energyunit_symbol(tostr)
    if from == :INVALID
        ArgumentError("'$(fromstr)' is not a valid unit specifier") |> throw
    elseif to == :INVALID
        ArgumentError("'$(tostr)' is not a valid unit specifier") |> throw
    end
    energyconvert(e, from, to)
end
energyconvert(e, p::Pair{<:AbstractString,<:AbstractString}) = energyconvert(e, p...)

function energyconvert(e, from::AbstractString)
    Energy(
        energyconvert(e, from => "J"),
        energyconvert(e, from => "kJ"),
        energyconvert(e, from => "J/mol"),
        energyconvert(e, from => "kJ/mol"),
        energyconvert(e, from => "cal"),
        energyconvert(e, from => "kcal"),
        energyconvert(e, from => "cal/mol"),
        energyconvert(e, from => "kcal/mol"),
        energyconvert(e, from => "eV"),
        energyconvert(e, from => "cm⁻¹"),
        energyconvert(e, from => "Hz"),
        energyconvert(e, from => "MHz"),
        energyconvert(e, from => "GHz"),
        energyconvert(e, from => "nm"),
    )
end


function energyunit_symbol(s)
    if s == "J" || s == "j"
        :J
    elseif s == "kJ" || s == "kj"
        :kJ
    elseif s == "J/mol" || s == "j/mol"
        :Jmol
    elseif s == "kJ/mol" || s == "kj/mol"
        :kJmol
    elseif s == "cal" || s == "Cal"
        :cal
    elseif s == "cal/mol" || s == "Cal/mol"
        :calmol
    elseif s == "kcal" || s == "kCal"
        :kcal
    elseif s == "kcal/mol" || s == "kCal/mol"
        :kcalmol
    elseif s == "eV" || s == "ev"
        :eV
    elseif s == "cm⁻¹" || s == "cm-1" || s == "wavenumber"
        :wavenumber
    elseif s == "Hz" || s == "s-1"
        :Hz
    elseif s == "MHz"
        :MHz
    elseif s == "GHz"
        :GHz
    elseif s == "nm"
        :nm
    else
        :INVALID
    end
end


function to_joule(from)
    if from == :J
        e -> e
    elseif from == :kJ
        e -> e*1e3
    elseif from == :Jmol
        e -> e/NA
    elseif from == :kJmol
        e -> e*1e3/NA
    elseif from == :cal
        e -> e*4.184
    elseif from == :calmol
        e -> e*4.184/NA
    elseif from == :kcal
        e -> e*1e3*4.184
    elseif from == :kcalmol
        e -> e*1e3*4.184/NA
    elseif from == :eV
        e -> e/6.241509074e18
    elseif from == :wavenumber
        e -> e*h*c*1e2
    elseif from == :Hz
        e -> e*h
    elseif from == :MHz
        e -> e*h*1e6
    elseif from == :GHz
        e -> e*h*1e9
    elseif from == :nm
        e -> h*c/e*1e9
    else
        ArgumentError("':$(String(from))' is not a valid unit specifier") |> throw
    end
end


function joule_to(to)
    if to == :J
        e -> e
    elseif to == :kJ
        e -> e*1e-3
    elseif to == :Jmol
        e -> e*NA
    elseif to == :kJmol
        e -> e*1e-3*NA
    elseif to == :cal
        e -> e/4.184
    elseif to == :calmol
        e -> e/4.184*NA
    elseif to == :kcal
        e -> e*1e-3/4.184
    elseif to == :kcalmol
        e -> e*1e-3/4.184*NA
    elseif to == :eV
        e -> e*6.241509074e18
    elseif to == :wavenumber
        e -> e/(h*c)*1e-2
    elseif to == :Hz
        e -> e/h
    elseif to == :MHz
        e -> e/h*1e-6
    elseif to == :GHz
        e -> e/h*1e-9
    elseif to == :nm
        e -> h*c/e*1e9
    else
        ArgumentError("':$(String(to))' is not a valid unit specifier") |> throw
    end
end


struct Mass
    au::Float64
    g::Float64
    kg::Float64
    gmol::Float64
    kgmol::Float64
end

function Base.show(io::IO, ::MIME"text/plain", m::Mass)
    print("UnitConversion.Mass:\n")
    floatpoint_alignment([
        m.au    => "atomic unit",
        m.g     => "g",
        m.kg    => "kg",
        m.gmol  => "g/mol",
        m.kgmol => "kg/mol"
    ])
end


function massconvert(m, from::Symbol, to::Symbol)
    if from == to
        m
    else
        m |> to_kg(from) |> kg_to(to)
    end
end
function massconvert(m, fromstr::AbstractString, tostr::AbstractString)
    from = massunit_symbol(fromstr)
    to = massunit_symbol(tostr)
    if from == :INVALID
        ArgumentError("'$(from)' is not a valid unit specifier") |> throw
    elseif to == :INVALID
        ArgumentError("'$(to)' is not a valid unit specifier") |> throw
    end
    massconvert(m, from, to)
end
massconvert(m, p::Pair{<:AbstractString,<:AbstractString}) = massconvert(m, p...)

function massconvert(m, from::AbstractString)
    Mass(
        massconvert(m, from => "au"),
        massconvert(m, from => "g"),
        massconvert(m, from => "kg"),
        massconvert(m, from => "g/mol"),
        massconvert(m, from => "kg/mol")
    )
end


function massunit_symbol(s)
    if s == "au" || s == "a.u." || s == "atomic unit"
        :au
    elseif s == "g"
        :g
    elseif s == "kg"
        :kg
    elseif s == "g/mol"
        :gmol
    elseif s == "kg/mol"
        :kgmol
    else
        :INVALID
    end
end


function to_kg(from)
    if from == :au
        m -> m*1e-3/NA
    elseif from == :g
        m -> m*1e-3
    elseif from == :kg
        m -> m
    elseif from == :gmol
        m -> m*1e-3/NA
    elseif from == :kgmol
        m -> m/NA
    else
        ArgumentError("':$(String(from))' is not a valid unit specifier") |> throw
    end
end


function kg_to(to)
    if to == :au
        m -> m*1e3*NA
    elseif to == :g
        m -> m*1e3
    elseif to == :kg
        m -> m
    elseif to == :gmol
        m -> m*1e3*NA
    elseif to == :kgmol
        m -> m*NA
    else
        ArgumentError("':$(String(to))' is not a valid unit specifier") |> throw
    end
end


struct Pressure
    Pa::Float64
    hPa::Float64
    MPa::Float64
    atm::Float64
    bar::Float64
    mbar::Float64
    mmHg::Float64
    torr::Float64
    psi::Float64
end

function Base.show(io::IO, ::MIME"text/plain", p::Pressure)
    print("UnitConversion.Pressure:\n")
    floatpoint_alignment([
        p.Pa   => "Pa",
        p.hPa  => "hPa",
        p.MPa  => "MPa",
        p.atm  => "atm",
        p.bar  => "bar",
        p.mbar => "mbar",
        p.mmHg => "mmHg",
        p.torr => "torr",
        p.psi  => "psi"
    ])
end


function pressureconvert(p, from::Symbol, to::Symbol)
    if from == to
        return p
    end
    return p |> to_Pa(from) |> Pa_to(to)
end
function pressureconvert(p, fromstr::AbstractString, tostr::AbstractString)
    from = pressureunit_symbol(fromstr)
    to = pressureunit_symbol(tostr)
    if from == :INVALID
        ArgumentError("'$(from)' is not a valid unit specifier") |> throw
    elseif to == :INVALID
        ArgumentError("'$(to)' is not a valid unit specifier") |> throw
    end
    return pressureconvert(p, from, to)
end
function pressureconvert(p, pair::Pair{<:AbstractString,<:AbstractString})
    return pressureconvert(p, pair...)
end
function pressureconvert(p, from::AbstractString)
    return Pressure(
        pressureconvert(p, from => "Pa"),
        pressureconvert(p, from => "hPa"),
        pressureconvert(p, from => "MPa"),
        pressureconvert(p, from => "atm"),
        pressureconvert(p, from => "bar"),
        pressureconvert(p, from => "mbar"),
        pressureconvert(p, from => "mmHg"),
        pressureconvert(p, from => "torr"),
        pressureconvert(p, from => "psi"),
    )
end


function pressureunit_symbol(s)
    if s == "Pa"
        return :Pa
    elseif s == "hPa"
        return :hPa
    elseif s == "MPa"
        return :MPa
    elseif s == "atm"
        return :atm
    elseif s == "bar"
        return :bar
    elseif s == "mbar"
        return :mbar
    elseif s == "mmHg"
        return :mmHg
    elseif s == "torr"
        return :torr
    elseif s == "psi"
        return :psi
    end
    return :INVALID
end


function to_Pa(unit)
    if unit == :Pa
        return p -> p
    elseif unit == :hPa
        return p -> p*100
    elseif unit == :MPa
        return p -> p*1_000_000
    elseif unit == :atm
        return p -> p*101_325
    elseif unit == :bar
        return p -> p*100_000
    elseif unit == :mbar
        return p -> p*100
    elseif unit == :mmHg
        return p -> p*101_325/760
    elseif unit == :torr
        return p -> p*101_325/760
    elseif unit == :psi
        return p -> p*6894.757293168
    end
    ArgumentError("':$(String(unit))' is not a valid unit specifier") |> throw
end


function Pa_to(unit)
    if unit == :Pa
        return p -> p
    elseif unit == :hPa
        return p -> p/100
    elseif unit == :MPa
        return p -> p/1_000_000
    elseif unit == :atm
        return p -> p/101_325
    elseif unit == :bar
        return p -> p/100_000
    elseif unit == :mbar
        return p -> p/100
    elseif unit == :mmHg
        return p -> p/(101_325/760)
    elseif unit == :torr
        return p -> p/(101_325/760)
    elseif unit == :psi
        return p -> p/6894.757293168
    end
    ArgumentError("':$(String(unit))' is not a valid unit specifier") |> throw
end


function unitconvert(x, fromstr::AbstractString, tostr::AbstractString)
    from = energyunit_symbol(fromstr)
    to = energyunit_symbol(tostr)
    if from != :INVALID && to != :INVALID
        return energyconvert(x, from, to)
    end
    from = massunit_symbol(fromstr)
    to = massunit_symbol(tostr)
    if from != :INVALID && to != :INVALID
        return massconvert(x, from, to)
    end
    from = pressureunit_symbol(fromstr)
    to = pressureunit_symbol(tostr)
    if from != :INVALID && to != :INVALID
        return pressureconvert(x, from, to)
    end
    ArgumentError("No available conversion from '$(fromstr)' to '$(tostr)'") |> throw
end
unitconvert(x, p::Pair{<:AbstractString,<:AbstractString}) = unitconvert(x, p...)

function unitconvert(x, fromstr::AbstractString)
    if energyunit_symbol(fromstr) != :INVALID
        return energyconvert(x, fromstr)
    end
    if massunit_symbol(fromstr) != :INVALID
        return massconvert(x, fromstr)
    end
    if pressureunit_symbol(fromstr) != :INVALID
        return pressureconvert(x, fromstr)
    end
    ArgumentError("'$(fromstr)' is not a valid unit specifier") |> throw
end


end # module
