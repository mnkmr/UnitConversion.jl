using UnitConversion
using Test

@testset "UnitConversion.jl" begin
    @test isapprox(energyconvert(1., "J" => "J/mol"),    6.02214076e23;  atol=1e15)
    @test isapprox(energyconvert(1., "J" => "kJ"),       1.0e-3;         atol=1e-16)
    @test isapprox(energyconvert(1., "J" => "kJ/mol"),   6.02214076e20;  atol=1e12)
    @test isapprox(energyconvert(1., "J" => "eV"),       6.241509074e18; atol=1e9)
    @test isapprox(energyconvert(1., "J" => "cm-1"),     5.03411657e22;  atol=1e14)
    @test isapprox(energyconvert(1., "J" => "nm"),       1.98644586e-16; atol=1e-16)
    @test isapprox(energyconvert(1., "J" => "cal"),      2.390e-1;       atol=1e-4)
    @test isapprox(energyconvert(1., "J" => "cal/mol"),  1.439e23;       atol=1e20)
    @test isapprox(energyconvert(1., "J" => "kcal"),     2.390e-4;       atol=1e-7)
    @test isapprox(energyconvert(1., "J" => "kcal/mol"), 1.439e20;       atol=1e17)

    @test isapprox(energyconvert(1., "J/mol"    => "J"), 1.66053907e-24; atol=1e-32)
    @test isapprox(energyconvert(1., "kJ"       => "J"), 1.0e3;          atol=1e-13)
    @test isapprox(energyconvert(1., "kJ/mol"   => "J"), 1.66053907e-21; atol=1e-29)
    @test isapprox(energyconvert(1., "eV"       => "J"), 1.60217663e-19; atol=1e-27)
    @test isapprox(energyconvert(1., "cm-1"     => "J"), 1.98644586e-23; atol=1e-31)
    @test isapprox(energyconvert(1., "nm"       => "J"), 1.98644586e-16; atol=1e-24)
    @test isapprox(energyconvert(1., "cal"      => "J"), 4.184;          atol=1e-4)
    @test isapprox(energyconvert(1., "cal/mol"  => "J"), 6.947e-24;      atol=1e-27)
    @test isapprox(energyconvert(1., "kcal"     => "J"), 4.184e3;        atol=1e1)
    @test isapprox(energyconvert(1., "kcal/mol" => "J"), 6.947e-21;      atol=1e-20)

    @test_throws ArgumentError energyconvert(1., "foo" => "J")
    @test_throws ArgumentError energyconvert(1., "J" => "foo")
    @test_throws ArgumentError energyconvert(1., "foo" => "foo")

    @test isapprox(massconvert(1., "kg" => "au"),     6.02214076e26; atol=1e18)
    @test isapprox(massconvert(1., "kg" => "g"),      1.0e3;         atol=1e-16)
    @test isapprox(massconvert(1., "kg" => "g/mol"),  6.02214076e26; atol=1e18)
    @test isapprox(massconvert(1., "kg" => "kg/mol"), 6.02214076e23; atol=1e18)

    @test isapprox(massconvert(1., "au"     => "kg"), 1.66053907e-27; atol=1e18)
    @test isapprox(massconvert(1., "g"      => "kg"), 1.0e-3;         atol=1e-16)
    @test isapprox(massconvert(1., "g/mol"  => "kg"), 1.66053907e-27; atol=1e-35)
    @test isapprox(massconvert(1., "kg/mol" => "kg"), 1.66053907e-24; atol=1e-32)

    @test_throws ArgumentError massconvert(1., "foo" => "kg")
    @test_throws ArgumentError massconvert(1., "kg" => "foo")
    @test_throws ArgumentError massconvert(1., "foo" => "foo")

    @test isapprox(unitconvert(1., "cal", "eV"), 2.611e19;      atol=1e16)
    @test isapprox(unitconvert(1., "g", "au"),   6.02214076e23; atol=1e18)

    @test_throws ArgumentError unitconvert(1., "J" => "kg")
    @test_throws ArgumentError unitconvert(1., "kg" => "J")
end
